# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'chimera/version'

Gem::Specification.new do |spec|
  spec.name          = "chimera"
  spec.version       = Chimera::VERSION
  spec.authors       = ["Sean Scally"]
  spec.email         = ["sean.scally@gmail.com"]
  spec.summary       = %q{Ruby wrapper for Dropwizard}
  spec.description   = %q{Makes Dropwizard more ruby-like}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'jbundler', '~> 0.7.2'
  spec.add_development_dependency 'bundler', '~> 1.7'
  spec.add_development_dependency 'rake', '~> 10.4.2'
  spec.add_development_dependency 'warbler', '~> 1.4.5'
  spec.add_dependency 'ruby-maven', '~> 3.1.1.0.11'
end
