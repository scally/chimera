java_import 'io.chimera.ShimApplication'
java_import 'org.slf4j.LoggerFactory'

class ChimeraApplication < ShimApplication
  class << self
    def main args
      new.java_send :run, [[].to_java(:string).class], args
    end

    def resource resource_class
      @@resources ||= []
      @@resources << resource_class
    end

    def health_check health_check_class
      @@health_checks ||= []
      @@health_checks << health_check_class
    end

    def resources *resource_classes
      resource_classes.each { |klass| resource klass }
    end

    def health_checks *health_check_classes
      health_check_classes.each { |klass| health_check klass }
    end

    def display_name name
      @@display_name = name
    end
  end

  def getName
    @@display_name || 'ChimeraApplication'
  end

  def ruby_run environment
    show_mission_critical_banner

    @@resources.each do |resource|
      resource.class_eval { become_resource }
      environment.jersey.register resource.java_class
    end

    @@health_checks.each do |health_check|
      # health_check.class_eval { become_java! }
      environment.health_checks.register 'basic', health_check.new
    end
  end

  def show_mission_critical_banner
    # MISSION CRITICAL BANNER FUNCTIONALITY until I fix #getResource
    LoggerFactory.getLogger(java_class).info File.readlines('public/banner.txt').unshift("\n").join
  end
end
