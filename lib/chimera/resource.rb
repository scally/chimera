java_import 'javax.ws.rs.GET'
java_import 'javax.ws.rs.Path'
java_import 'javax.ws.rs.Produces'
java_import 'javax.ws.rs.core.MediaType'

module Resource
  def path url
    @resource_url = url
  end

  def respond_with media
    @resource_respond_with = media
  end

  def action method_name
    @current_method_name = method_name
    if block_given?
      yield self
    else
      get
      returns Hash
    end
  end

  def get
    add_method_annotation @current_method_name, GET => {}
  end

  def returns klass
    add_method_signature @current_method_name, [klass.new.to_java.class]
  end

  def become_resource
    add_class_annotations media_to_produce_annotation (@resource_respond_with || :json)

    # Replace with Pluralize from ActiveSupport
    add_class_annotations Path => {'value' => (@resource_url || name.gsub(/resource/i, '').downcase )}

    instance_methods.grep(/^index/).each do |index_method|
      action index_method.to_sym
    end

    become_java!
  end

  def media_to_produce_annotation media
    case media
    when :text
      {Produces => {'value' => ['text/plain'].to_java(:string)}}
    when :json
      {Produces => {'value' => [MediaType::APPLICATION_JSON].to_java}}
    else
      raise "Don't know how to produce '#{media}'"
    end
  end
end
