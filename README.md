# Chimera

Ruby wrapper for Dropwizard

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'chimera'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install chimera

## Usage

Not very sophisticated yet

1. clone http://github.com/scally/hello_chimera
2. customize to taste

## Contributing

1. Fork it ( https://github.com/[my-github-username]/chimera/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
