package io.chimera;

import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public abstract class ShimApplication extends Application<Configuration> {
  @Override
  public void initialize(Bootstrap<Configuration> bootstrap) {
  }

  @Override
  public void run(Configuration configuration, Environment environment) throws Exception {
      ruby_run(environment);
  }

  public abstract void ruby_run(Environment environment);
}
